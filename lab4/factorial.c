#include <stdio.h>
#include <limits.h>
#include <math.h>

long maxlong(void){
	return LONG_MAX;
}

double upper_bound(long n){
	double x;
	if(n<6){
		x=719;
	}
	else{
		x=pow((n/2.0),n);
	}
	return x;
}

long factorial(long n){
	long fact=1.0;
	if(n<0){
		fact=-2;
	}
	else if(upper_bound(n)<maxlong()){
		int count;
			for (count=1;count<=n;count++){
				fact=fact*count;
			}
	}
	else {
		fact=-1;
	}
	return fact;
}

int main(void) {
    long i;

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */

    
    for (i=0; i<10; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
        printf("factorial(%ld)=%ld\n", i, factorial(i));		
    }
    return 0;
}

